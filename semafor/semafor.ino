// declaring macros for accesing proper pins
#define button  7
#define carRed 2
#define carYell 3
#define carGreen 4
#define pedRed 5
#define pedGreen 6

//how long pedestrian green will be on
#define crossTime 3000

//time from previous pedestrian crossing
unsigned long changeTime;

void changeLights();

void setup() {
  // setuping pins
  pinMode(button,INPUT);
  Serial.begin(9600);
  changeTime = millis();
  digitalWrite(carRed,LOW);
  digitalWrite(carYell,LOW);
  digitalWrite(carGreen,HIGH);
  digitalWrite(pedRed,HIGH);
  digitalWrite(pedGreen,LOW);
}

void loop() {
  int state = digitalRead(button);
  Serial.println(state);
  if(state == HIGH && millis()-changeTime > 10000){
    changeLights();
  } 
}

void changeLights(){
  delay(500);
  // stop cars
  digitalWrite(carRed,HIGH);
  digitalWrite(carGreen,LOW);
  delay(300);
  // let pedestrians cross
  digitalWrite(pedRed,LOW);
  digitalWrite(pedGreen,HIGH);
  delay(crossTime);

  // blink green to let the pedestrians know that green will end soon
  for(int i = 0; i<5; i++){
    digitalWrite(pedGreen,LOW);
    delay(100);
    digitalWrite(pedGreen,HIGH);
    delay(100);
  }
  // stop pedestrains, notify cars with yellow to get ready
  digitalWrite(pedRed,HIGH);
  digitalWrite(pedGreen,LOW);
  digitalWrite(carYell,HIGH);
  delay(1000);
  
  // let cars go
  digitalWrite(carRed,LOW);
  digitalWrite(carYell,LOW);
  digitalWrite(carGreen,HIGH);
  changeTime = millis();
}