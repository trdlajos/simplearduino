// ultrasound sensor gets a signal from arduino - trigpin, sends back signal - echopin
// pulseIn measures how long it takes sound to travel to and back from object through amount of time ECHO is on HIGH. 
//ECHO pin goes HIGH when transmission of 8 burst of sound from sensor is finished, and goes LOW when recieved
#define ECHOPIN 11
#define TRIGPIN 12
#define MINDIST 15 

// pitches can be used to buzz with buzzer easily on various frequencies
#include "pitches.h"

void setup() {
  Serial.begin(9600);
  pinMode(ECHOPIN,INPUT);
  pinMode(TRIGPIN,OUTPUT);
  digitalWrite(TRIGPIN,LOW);
}

int intensity = 0;

void loop() {  
  digitalWrite(TRIGPIN,HIGH);
  delayMicroseconds(10);
  digitalWrite(TRIGPIN,LOW);
  float distance = pulseIn(ECHOPIN,HIGH);
  // from time get distance
  distance = distance*0.017315f;
  Serial.print(distance);
  Serial.print("\n");
  if(distance < MINDIST){
    // if distance is smaller than certain threshold, play a tone on pin 3 for duration of 100 ms
    tone(3, NOTE_D5,100);
  }
  delay(1000);
}