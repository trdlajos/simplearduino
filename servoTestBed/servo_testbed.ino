#include <Servo.h>

// pins
#define buttonPin 2
#define vPin A2
#define servoPin 9

// constants
#define minPressTime 500
#define stateNum 3

// LED current state indication
#define indicateStates true
#define startPin 2

//number of previous readings to be taken into account in smoothing
#define parameterCount 60

// frequencies of main program
#define debug_frequency 1000
#define motorKnob_frequency 10
#define sweepdel_frequency 400

#define show_debug true

// global vars - state handling
int state = 0; 
unsigned long press_start;
bool pressed  = false;

// timers

// timer for sending debug info
unsigned long custom_timerP = 0;
// timer for updating motor commands - potenciometer(knob)
unsigned long custom_timerK = 0;
// timer for delaying motor readings - automatic sweep
unsigned long custom_timerS = 0;


/*program has two states 
0 - knob control 
1 - sweeping - one
2 - sweeping continous 
3 - manual ?? coming soon 
*/ 


// smoothing variables
int values_list[parameterCount];
unsigned long sum = 0;
int index_vlist = 0;

// servo control

Servo sr;
bool oneSweepEnd = false;
bool contSweepStart = false; 
int sweepBoundary = 180;


bool change_state();
// antibouncing button listener that returns true each time button is pressed and released, pressed has to be longer than minPressTime 

void update_state(bool indicate);
// if called, increments accordingly to stateNum the state variable. If indicate set to true, sets pin on location state+pinOffset to HIGH, previous pin to LOW
// note that pins have to have correct pinMode

void setup_indication(bool indicate);
// in case indication of current state by LED is needed, this function sets up the proper pinModes on pins corresponding to states from 2 to stateNum. 

void update_state_params();
// if is is needed to reinit/change some variables each time state is changed, do it inside this function

float get_average(int value, int length);
// calculates moving average by replacing values in values_list  

void do_knob_control(int value, int timer_length);
// knob control - servo angle is controlled accordingly to potenciometr

void do_oneSweep();
// does only one sweep back and forth, then stops

void do_contSweep(int timer_length);
// sweeps back and forth continously  

void init_values_list(int length);
// inits values_list with zeros

void debug(int frequency, bool show, int value);
// simple debug method that prints to serial output if show is true accordingly to provided frequency 

void setup() {
  // state and input setup
  pinMode(buttonPin, INPUT);
  pinMode(vPin,INPUT);  
  setup_indication(indicateStates);
  init_values_list(parameterCount);

  //servo setup 
  sr.attach(servoPin);

  // debug 
  Serial.begin(9600);

}

void loop() {

  // if change of state occured, update mode
  if (change_state()){
    update_state(indicateStates);
    update_state_params();
    //init_values_list(parameterCount);
  }

  int value = get_average(analogRead(vPin),parameterCount);

  debug(debug_frequency,show_debug,value);

  // send order to motors accordingly to selected state
  switch (state) {
    case 2:
      do_contSweep(sweepdel_frequency);
    break;
    case 1: 
      if(oneSweepEnd == false){
        do_oneSweep();
      }
    break;
    case 0: 
      //knob control 
      do_knob_control(value,motorKnob_frequency);
    break;
    default:
      Serial.print("def");
      Serial.println(state);
      break;
  } 
}

bool change_state(){

  bool change = false;

  if(digitalRead(buttonPin) == HIGH && !pressed){
      // rising edge, start counter
      press_start = millis();
      pressed = true; 
  }
  else if(digitalRead(buttonPin) == LOW && pressed){
    // droping edge, check if press was long enough to change state to prevent bouncing
    if(press_start-millis()>=minPressTime){
      pressed = false;
      change = true;
    }
    pressed = false;
  }
  return change;
}

void update_state(bool indicate){
    if(state > 0 && indicate){
      // off state indicator
      digitalWrite(state+startPin,LOW);
    }

    state++;
    if(state>=stateNum){
      state = 0;
    }
    
    if(state > 0 && indicate){
      // off state indicator
      digitalWrite(state+startPin,HIGH);
    }
}

void update_state_params(){
  contSweepStart = false;
  oneSweepEnd = false;
}

void setup_indication(bool indicate){
  if(indicate){
  for(int pin = startPin+1;pin<2+stateNum;pin++){
    pinMode(pin, OUTPUT);
    }
  }
}

float get_average(int value, int length){
  sum = sum - values_list[index_vlist] + value;
  values_list[index_vlist] = value;
  index_vlist++;
  if(index_vlist >=length){
    index_vlist = 0;
  }
  return sum/length;
}

void init_values_list(int length){
  for(int i = 0;i<length;i++){
    values_list[i] = 0;
  }
}

void do_knob_control(int value, int timer_length){
  if(millis()-custom_timerK>=timer_length){
    int angle = map(value, 0, 1023,0,180);
    sr.write(angle);
    custom_timerK = millis();
  }
}

void do_oneSweep(){
  for (int pos = 0; pos <= 180; pos += 1) { 
    sr.write(pos);                  
    delay(15);                      
  }
  for (int pos = 180; pos >= 0; pos -= 1) { 
    sr.write(pos);                  
    delay(15);                      
  }
  oneSweepEnd = true;  
}

void debug(int frequency, bool show, int value){
  if(millis()-custom_timerP>=frequency){
    Serial.print("State: ");
    Serial.println(state);
    Serial.print("Measurement: "); 
    Serial.println(value);
    Serial.print("Motor: "); 
    Serial.println(sr.read());
    Serial.println();

    custom_timerP = millis();
  }
}

void do_contSweep(int timer_length){
  if(millis()-custom_timerS>=timer_length){
    if(contSweepStart == false){
      // start sweeping
      sr.write(sweepBoundary);
      contSweepStart = true;
    }

    if(sr.read() == 0 || sr.read() == 180){
      // wall hitted
      sweepBoundary = -sweepBoundary;
      sr.write(sweepBoundary);
      delay(400);
    }
    custom_timerS = millis();
  }
}
