
// pins
#define buttonPin 2
#define vPin A2

// constants
#define minPressTime 500
#define stateNum 3

// LED current state indication
#define indicateStates true
#define startPin 2

//number of previous readings to be taken into account, ordered from highest to smallest
#define parameterCountA 30
#define parameterCountB 15
#define parameterCountC 1

// frequency of taking samples
#define customSamp_frequency true
#define sample_period 100
#define debug_period 1000

#define show_debug true

// global vars
int state = 0; 
unsigned long press_start = 0;
unsigned long sample_timer = 0;
unsigned long debug_timer = 0;
bool pressed  = false;
int value = 0;

// smoothing variables
int values_list[parameterCountA];
unsigned long sum = 0;
int index_vlist = 0;
int movingAv_num = parameterCountA;

bool change_state();
// antibouncing button listener that returns true each time button is pressed and released, pressed has to be longer than minPressTime 

void update_state(bool indicate);
// if called, increments accordingly to stateNum the state variable. If indicate set to true, sets pin on location state+pinOffset to HIGH, previous pin to LOW
// note that pins have to have correct pinMode

void setup_indication(bool indicate);
// in case indication of current state by LED is needed, this function sets up the proper pinModes on pins corresponding to states from 2 to stateNum. 

float get_average(int value, int length);
// calculates moving average by replacing values in values_list 

float perform_measurement(int period, int sample_pin, int previous_val);
// takes a sample according to period specified

void init_values_list(int length);
// inits values_list with zeros

void debug(int period, bool show, int value);
// prints debug in given period



void setup() {
  pinMode(buttonPin, INPUT);
  pinMode(vPin,INPUT);
  setup_indication(indicateStates);
  init_values_list(parameterCountA);

  // debug 
  Serial.begin(9600);
}

void loop() {

  // if change of state occured, update mode
  if (change_state()){
    update_state(indicateStates);
    
    // reinit smoothing variables
    init_values_list(parameterCountA);
    sum = 0;
    index_vlist = 0;

  }

  //measure
  value = perform_measurement(sample_period, vPin, value);
  debug(debug_period, show_debug, value);

  //set smoothing coeff accordingly to state
  switch(state){
  case 0: 
  movingAv_num = parameterCountA;
  break;
  case 1: 
  movingAv_num = parameterCountB;
  break;
  case 2: 
  movingAv_num = parameterCountC;
  break;
  }
}


bool change_state(){

  bool change = false;

  if(digitalRead(buttonPin) == HIGH && !pressed){
      // rising edge, start counter
      press_start = millis();
      pressed = true; 
  }
  else if(digitalRead(buttonPin) == LOW && pressed){
    // droping edge, check if press was long enough to change state to prevent bouncing
    if(press_start-millis()>=minPressTime){
      pressed = false;
      change = true;
    }
    pressed = false;
  }
  return change;
}

void update_state(bool indicate){
    if(state > 0 && indicate){
      // off state indicator

      digitalWrite(state+startPin,LOW);
    }

    state++;
    if(state>=stateNum){
      state = 0;
    }
    
    if(state > 0 && indicate){
      // off state indicator
      digitalWrite(state+startPin,HIGH);
    }
}

void setup_indication(bool indicate){
  if(indicate){
  for(int pin = startPin+1;pin<2+stateNum;pin++){
    pinMode(pin, OUTPUT);
    }
  }
}

float get_average(int value){
  sum = sum - values_list[index_vlist] + value;
  values_list[index_vlist] = value;
  index_vlist++;
  if(index_vlist >=movingAv_num){
    index_vlist = 0;
  }
  return sum/movingAv_num;
}

void init_values_list(int length){
  for(int i = 0;i<length;i++){
    values_list[i] = 0;
  }
}
void debug(int period, bool show, int value){
    if(millis()-debug_timer>=period && show){
    Serial.print("State: ");
    Serial.println(state);
    Serial.print("Measurement: "); 
    Serial.println(value);
    Serial.print("Moving average param: "); 
    Serial.println(movingAv_num);

    debug_timer = millis();
  }
}

float perform_measurement(int period, int sample_pin, int previous_val){
    if(customSamp_frequency){
      if(millis()-sample_timer>=sample_period){
        sample_timer = millis();
        //Serial.println("sampling... ");
        return get_average(analogRead(sample_pin));
      }
      else{
        return previous_val;
      }
    }
    else{
      return get_average(analogRead(sample_pin));
      }
  }