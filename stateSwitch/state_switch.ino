// pins
#define buttonPin 2

// constants
#define minPressTime 300
#define stateNum 3

// LED current state indication
#define indicateStates true
#define startPin 2

// timers
#define main_frequencyP 1000

//debug 
#define show_debug true

// timer for sending debug info
unsigned long custom_timerP = 0;

// global vars
int state = 0;
unsigned long press_start;
bool pressed = false;

bool change_state();
// antibouncing button listener that returns true each time button is pressed and released, pressed has to be longer than minPressTime

void update_state(bool indicate);
// if called, increments accordingly to stateNum the state variable. If indicate set to true, sets pin on location state+pinOffset to HIGH, previous pin to LOW
// note that pins have to have correct pinMode

void setup_indication(bool indicate);
// in case indication of current state by LED is needed, this function sets up the proper pinModes on pins corresponding to states from 2 to stateNum.

void debug(int frequency, bool show);
// simple debug method that prints to serial output if show is true accordingly to provided frequency

void setup() {
  pinMode(buttonPin, INPUT);
  setup_indication(indicateStates);

  // debug
  Serial.begin(9600);

  // put your setup code here, to run once:
  
}

void loop() {

  // if change of state occured, update mode
  if (change_state()) {
    update_state(indicateStates);
  }

  // debug
  debug(main_frequencyP, show_debug);

  // put your loop code here
}

bool change_state() {
  bool change = false;

  if (digitalRead(buttonPin) == HIGH && !pressed) {
    // rising edge, start counter
    press_start = millis();
    pressed = true;
  }

  else if (digitalRead(buttonPin) == LOW && pressed) {
    // falling edge, check if press was long enough to change state to prevent bouncing
    if (press_start - millis() >= minPressTime) {
      pressed = false;
      change = true;
    }
    pressed = false;
  }
  return change;
}

void update_state(bool indicate) {
  if (state > 0 && indicate) {
    // off state indicator
    digitalWrite(state + startPin, LOW);
  }

  state++;
  if (state >= stateNum) {
    state = 0;
  }

  if (state > 0 && indicate) {
    // on state indicator
    digitalWrite(state + startPin, HIGH);
  }
}

void setup_indication(bool indicate) {
  if (indicate) {
    for (int pin = startPin + 1; pin < 2 + stateNum; pin++) {
      pinMode(pin, OUTPUT);
    }
  }
}

void debug(int frequency, bool show) {
  if (millis() - custom_timerP >= main_frequencyP) {
    Serial.print("State: ");
    Serial.println(state);
    Serial.println();

    custom_timerP = millis();
  }
}
