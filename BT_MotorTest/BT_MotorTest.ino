
//ESP32 bluetooth motor tester for NIDEC 24H
//by JT

/*
Motor notes:
3 controlling inputs(PWM, direction and brake) and 2 optical encoder outputs
Increase in duty cycle means decrease in velocity!

Commands:
h - help

r - starts motor
s - stops all operation
m - motor status
+ - increases velocity(decreases dutycycle)
- - decreases velocity()
c - change polarity(reverse)

*/


#include "BluetoothSerial.h"

#define DEBUG false

// Bluetooth macros
#define NAME "JT_ESP_MOTOR_TEST"
#define CH_CONN true // if true, ESP will broadcast with MSG_P period a message over BT
#define MSG_P 100000

//pin macros
#define pwmP 33
#define brakeP 27
#define dirP 32
#define chA 25
#define chB 26

//motor macros
#define MAX_SPEED_REL 8
#define MIN_SPEED_REL 1
#define SPEED_STEP 1

// custom frequency 
#define CUSTOM_F true
#define FREQ 16000
#define PWM_CH 0 
#define RES 3

#define RELATIVE_RES 255/8

// time vars
unsigned long last_t = 0; 

// control vars
bool new_msg = false;
char msg; 

//motor vars
bool start = false;
bool turn_cw = true;
float pwmDutyRel = MIN_SPEED_REL; 


// Check if Bluetooth configs are enabled
#if !defined(CONFIG_BT_ENABLED) || !defined(CONFIG_BLUEDROID_ENABLED)
#error Bluetooth is not enabled! Please run `make menuconfig` to and enable it
#endif

// parses input to proper form
char handle_input();
// does commands accordingly to user input
void do_command(char msg);
// updates motor controlling pins
void update_motor();
// prints commands help
void print_help();
// prints status of motor
void print_motor_status();
// debug func 
void print_debug(bool debug, char msg);

BluetoothSerial SBT;

void setup() {
  //comm
  Serial.begin(115200);
  SBT.begin(NAME);

  //pins
  pinMode(pwmP,OUTPUT);
  pinMode(dirP,OUTPUT);
  pinMode(brakeP,OUTPUT);
  if(CUSTOM_F){
    ledcAttachChannel(pwmP,FREQ,RES,PWM_CH);
  }

}

void loop() {

  // listen to commands from controlling device
  if(SBT.available()){
    msg = handle_input();
    new_msg= true; 
  }

  // execute desired command, update motor pins
  if(new_msg){
    do_command(msg);
    print_debug(DEBUG,msg);
    update_motor();
    new_msg = false;
  }


  // print MSG with a defined interval
  if(millis()-last_t > MSG_P && CH_CONN){
    SBT.println("Use h for help!");
    last_t = millis();
    }
}

void do_command(char msg){
switch(msg){
  case 'r':
    start = true;
    break;
  case 's':
    start = false;
    break;
  case '-':
    pwmDutyRel += SPEED_STEP;
    if(pwmDutyRel>MAX_SPEED_REL){
      pwmDutyRel = MAX_SPEED_REL;
    }
    break;
  case '+':
    pwmDutyRel -= SPEED_STEP;
    if(pwmDutyRel<MIN_SPEED_REL){
      pwmDutyRel = MIN_SPEED_REL;
    }
    break;
  case 'c':
    turn_cw = !turn_cw;
    break;
  case 'h':
    print_help();
    break;
  case 'm':
    print_motor_status();
    break;
  default: 
    SBT.println("Bad message");
  }
}

void print_debug(bool debug, char msg){
  if(debug){
    switch(msg){
      case 'r':
        Serial.printf("run: ");
        Serial.println(start);
        break;
      case 's':
        Serial.printf("stop: ");
        Serial.println(start);
        break;
      case '+':
        Serial.printf("+: ");
        Serial.println(pwmDutyRel);
        Serial.printf("Absolute: ");
        Serial.println(pwmDutyRel*RELATIVE_RES);
        break;
      case '-':
        Serial.printf("-: ");
        Serial.println(pwmDutyRel);
        Serial.printf("Absolute: ");
        Serial.println(pwmDutyRel*RELATIVE_RES);
        break;
      case 'c':
        Serial.printf("rev: ");
        Serial.println(turn_cw);
        break;
    }
  }
}

void update_motor(){
  // update motor pins
  if(!DEBUG){
    digitalWrite(brakeP, start);
    digitalWrite(dirP, turn_cw);
    SBT.println("Motors updated!");
    
    // if want to use custom frequency
    if(CUSTOM_F){
      ledcWriteChannel(PWM_CH,pwmDutyRel);
    }else{
      analogWrite(pwmP, pwmDutyRel*RELATIVE_RES);
    }

  }
  else{Serial.println("Motors updated!"); 
  }
}

char handle_input(){
  // reads only first char on the line
  String inputString = "";
  char fChar = SBT.read();
  // Read until newline character or timeout, 
  while (SBT.available()) {
    if (SBT.read() == '\n') {
      break;  // Stop reading when newline is encountered
    }
    delay(10);  // Short delay to allow more data to be received
  }
  return fChar;
}

void print_help(){
  SBT.println("r/s ... run/stop the motor");
  SBT.println("c ... reverse - changes polarity");
  SBT.println("+/- ... increases/decreases speed");
  SBT.println("m ... status report of motor");
  SBT.print("Using custom frequency: ");
  SBT.println(CUSTOM_F ? "Y!" : "N");
}
void print_motor_status(){
  SBT.print("Enabled: ");
  SBT.println(start);
  SBT.print("Polarity: ");
  SBT.println(turn_cw ? "CW" : "CCW");
  SBT.println("Speed: ");
  SBT.println(pwmDutyRel);
}
