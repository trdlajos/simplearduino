
#include <Stepper.h>


#define motor_steps 64 // native steps of rotor inside motor
#define gearbox_ratio 32 // gearbox ratio
int total_steps = motor_steps*gearbox_ratio;

#define in1 = 2
#define in2 = 3
#define in3 = 4
#define in4 = 5

int previous_val = 0;
int max_speed = 100; // dunno real max speed, try something higher

Stepper motor(32,in1,in3,in2,in4);

// angle of rotor controlled through potenciometr output
void set_angle(int value);
// speed of rotor controlled through potenciometr output
void set_speed(int value);

#define v_pin = A2 

float step_res = (gearbox_ratio*motor_steps)/1024

// we want to turn in both directions 
float speed_res = 512/max_speed;

void setup() {
  pinMode(v_pin, INPUT);
  motor.setSpeed(80);
  //debug 
  //Serial.begin(9600);
}

void loop() {
  int value = analogRead(v_pin);
  // set angle to new value, 
  set_angle(value-previous_val);
  previous_val=value;
  //debug 
  //Serial.println(value);
  delay(500);

}

void set_angle(int value){
  // sets motor angle (step number to turn to) accordingly to 
  // voltage measured on potencimeter
  
  int steps = value*step_res;
  motor.step(steps);
}

void set_speed(int value){
  // setting speed according to measured voltage on potenciometer, 
  // big zone of insensitivity had to be used beacause of high variation in 
  // value variable
  int speed = 0;
  
  if(value > 522){
    // turn positive
    value = value - 512;
    speed = value / speed_res;
  }
  else if(value < 502){
    // turn negative
    value = -value;
    speed = value / speed_res;
  } 
  motor.setSpeed(speed);
}

