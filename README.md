Small arduino projects done in summer 2024 as part of my [Becoming maker adventure](https://blog-trdlajos-c7b49fd50c516e60f4e2487ddf12a647deff0989b084224ea.pages.fel.cvut.cz/#scroll). 

## List of projects: 

### Semafor
Simple program controlling 5 LEDs - 3 LEDs emulating car semafor and 2 LEDs emulating pedestrian semafor. 

### Ultrasonic sensor with a buzzer 
Well-known combination from cars.

### Segment display and LED matrix 
Using arduino as voltmeter measuring a potenciometr. As output is used 7-segment display and LED matrix. Custom arduino libraries has been used: [LEDControl for MAX7219](http://wayoda.github.io/LedControl/) and [TM1637](https://github.com/avishorp/TM1637/blob/master/README.md).

### State switcher
Template for arduino programs where user can with button cycle through various states of the program. Number of states is parametrized. Has build in LED indication of current state that can be turned off. Possible use case is shown on smoothing example, where user can pick between smoothing strongness applied on sensor data. (essentially number of samples used in moving average) This example can be used for testing various sensor inputs. 

### Servo testbed 
Simple algorithm for testing servos, needed for Servoproject. Has three modes of operation - manual control, one slow sweep and automatic continous sweeping. Note having same voltage source for motor and potenciometr can cause problems, as motor can affect voltage. 

### NIDEC 24H tester(BT_motorTest)
Simple program for testing 24H motors. Utilizes ESP32 bluetooth capabilities for sending commands to motors. 

