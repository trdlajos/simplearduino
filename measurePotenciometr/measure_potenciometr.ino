#include "LedControl.h"
#include "TM1637Display.h"


/* Create a new LedControl variable.
parameters: data pin, CLK pin, CS pin, number of matrices attached
 */  

#define DATA_mat = 11
#define CLK_mat = 13
#define CS_mat = 10
#define led_count = 64

LedControl ledb = LedControl(DATA_mat,CLK_mat,CS_mat,1); 

/*
Creating a new TM1637Display variable.
parameters: CLK pin, DATA  pin
*/
#define CLK_seg 2
#define DIO_seg 3

TM1637Display disp = TM1637Display(CLK_seg, DIO_seg);

void update_lights(int led_num);
void update_disp(float number);

// measuring voltage pin and resolution of one LED in matrix 
#define vPin = A2; 
int reso = 1024/led_count;

// possible LED-on distribution of row in LED matrix stored as array, index corresponds to number of LEDS lit
char led_rows[] = {B00000000,B10000000,B11000000,B11100000,B11110000,B11111000,B11111100,B11111110,B11111111};

void setup() {
  pinMode(vPin,INPUT);
  Serial.begin(9600);
  ledb.shutdown(0,false);
  ledb.setIntensity(0,2);
  ledb.clearDisplay(0);  
  disp.setBrightness(10);
}

void loop() {
  int value = analogRead(vPin);
  //convert to voltage 
  float voltage = value*(0.00488);

  //get nbumber of LEDS to lit
  int led_num = value/reso;
  
  // print in serial monitor for DEBUG
  Serial.print("Reading: ");
  Serial.println(value);
  Serial.print("LEDS: ");
  Serial.println(led_num);
  Serial.print("Voltage: ");
  Serial.println(voltage);

  // update output peripherals
  update_lights(led_num);
  update_disp(voltage);

  delay(250);
}

void update_lights(int led_num){
  // according toi number of LEDs to be lit, divide by 8 to get number of whole rows to be lit and lit the rest using led_rows array 
  int rows_num = led_num/8;
  for(int i = 0; i<8;i++)
    {
      if(i<rows_num){
      ledb.setRow(0, i, led_rows[8]);
      }
      else if(i>rows_num)
      {
        ledb.setRow(0, i, led_rows[0]);
        }
      }
    int rest = led_num - rows_num*8;
    ledb.setRow(0,rows_num,led_rows[rest]);
    }

void update_disp(float number){
  // use two decimals
  int res = number*100;
  disp.showNumberDec(res,true);
}
